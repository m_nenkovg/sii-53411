#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"
#include <unistd.h>
#include <stdlib.h>


int main(int argn, char* argv[]){

	DatosMemCompartida* pmem;
	int fd;	

	if(argn < 2){
		printf("Error de ejecucion: bot dificultad");
		exit (0);
	}
	
	fd = open("/tmp/mmap_bot", O_RDWR);
	pmem = (DatosMemCompartida *) mmap(NULL, sizeof(&pmem), PROT_READ|PROT_WRITE, MAP_SHARED,fd,0);
	close(fd);

	useconds_t time = 25 * 1000;
	switch(atoi(argv[1])){
                        case 0: pmem->accion = 0;
                                exit(0);
                        case 1: time = 2 * 1000*1000;
                                break;
                        case 2: time = 1.5 * 1000*1000;
                                break;
                        case 3: time = 1 * 1000*1000;
                                break;
                        case 4: time = 25 * 1000;
                                break;
                        default: time = 2 *1000*1000; 
                }
	while(1){
		usleep(time);
		if(pmem->esfera.centro.y > (pmem->raqueta1.y1 + pmem->raqueta1.y2)/2 )
			pmem->accion = 1;
		else if(pmem->esfera.centro.y < (pmem->raqueta1.y1 + pmem->raqueta1.y2)/2 )
			pmem->accion = -1;
		else
			pmem->accion = 0;
	};
	return 0;
}
