#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(){
	
	char* fifo = "/tmp/fifo_servidor";
	mkfifo(fifo, 0666);
	int fd, n;

	fd = open(fifo, O_RDONLY);
	int info[2], puntos1, puntos2;
	puntos1 = puntos2 = 0;
	while(1){
		n = read(fd,&info,sizeof(info));
		if(n < 0){
			perror("Lectura incorresta: ");
			return 0;
		}
		else if(n >= 0){
			if(info[0] == 1){
				puntos1 = info[1];
				if(info[1] == 4){ 
                                        printf("Jugador 1 ha ganado\n");
                                        break;
                                };
				printf("Jugador 1 ha anotado, ahora tiene %d puntos.\n",info[1]);
			}
			else if(info[0] == 2){
				puntos2 = info[1];
				if(info[1] == 4){
					printf("Jugador 2 ha ganado\n");
					break;
				};
				printf("Jugador 2 ha anotado, ahora tiene %d puntos.\n",info[1]);
			};
		}
		else{
			continue;
		};
	};
	close(fd);
	return 0;
}
